//
//  ColorHelper.swift
//  TapRace
//
//  Created by Felipe Sousa on 1/23/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation

class ColorHelper {
    
    let COLORSET = [0:["#6985B3", "#506689"], 1:["#5DAD9D","#488479"], 2: ["#FFBB8A","#C5906A"], 3:["#F88895","#BF6872"]]
    
    class func getInstance() -> ColorHelper {
        struct Static {
            static let instance : ColorHelper = ColorHelper()
        }
        return Static.instance
    }
    
    func getRandomSetColor() -> [String] {
        let randomIndex = Int(arc4random_uniform(UInt32(COLORSET.count)))
        return COLORSET[randomIndex]!
    }
}