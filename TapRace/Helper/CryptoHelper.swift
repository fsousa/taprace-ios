//
//  CryptoHelper.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/18/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation

class CryptoHelper {
    
    private var key = "Gfg`MC7o_vPQ1ccBSfKB7_HfOd76Nkqd".encodeWithXorByte(5)
    private var iv = "1tCaWTILjA}0FR22".encodeWithXorByte(4)
    
    private var keyUserAgent = "_k2gVbTtPlWdtW1T@\\s`Kd?lP3_HVm>|".encodeWithXorByte(6)
    private var ivUserAgent = "140nNQi;tGtgZr;n".encodeWithXorByte(3)
    
    private var keyLeaderboard = "OX4E5f:xc6laZAHXn1I:{OVp2AQ7I4rm".encodeWithXorByte(2)
    private var ivLeaderboard = "dE:5b2bqoKEIQYmA".encodeWithXorByte(3)
    
    private var keyFix = "r=d23A|}|NVP`_O`".encodeWithXorByte(5)
    private var keyUserAgentFix = "[4f1CQUEr:X60At4".encodeWithXorByte(2)
    
    class func getInstance() -> CryptoHelper {
        struct Static {
            static let instance : CryptoHelper = CryptoHelper()
        }
        return Static.instance
    }
    
    func encrypt(plain: String) -> String {
        return try! plain.aesEncrypt(self.key, iv: self.iv)
    }
    
    func dencrypt(cipher: String) -> String {
        return try! cipher.aesDecrypt(self.key, iv: self.iv)
    }
    
    func encryptUserAgent(plain: String) -> String {
        return try! plain.aesEncrypt(self.keyUserAgent, iv: self.ivUserAgent)
    }
    
    func dencryptUserAgent(cipher: String) -> String {
        return try! cipher.aesDecrypt(self.keyUserAgent, iv: self.ivUserAgent)
    }
    
    func encryptLeaderboardUpdate(plain: String) -> String {
        return try! plain.aesEncrypt(self.keyLeaderboard, iv: self.ivLeaderboard)
    }
    
    func dencryptLeaderboardUpdate(cipher: String) -> String {
        return try! cipher.aesDecrypt(self.keyLeaderboard, iv: self.ivLeaderboard)
    }
    
    func getKeyFix() -> String {
        return self.keyFix
    }
    
    func getKeyFixUserAgent() -> String {
        return self.keyUserAgentFix
    }
    
    
}