//
//  StorageHelper.swift
//  TapRace
//
//  Created by Felipe Sousa on 1/23/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation


class StorageHelper {
    
    let RECORDKEY = "com.fousasilva.taprace.record"
    let SYNCLATERKEY = "com.fsousasilva.taprace.sendLater"
    let TOKEN = "com.fsousasilva.taprace.token"
    let FIRSTRUN = "com.fsousasilva.taprace.firstrun"
    
    let USERNAME = "com.fsousasilva.taprace.username"
    
    class func getInstance() -> StorageHelper {
        struct Static {
            static let instance : StorageHelper = StorageHelper()
        }
        return Static.instance
    }
    
    func saveRecord(value : Int) {
        saveInt(value, key: RECORDKEY)
    }
    
    func readRecord() -> Int {
        return readInt(RECORDKEY)
    }
    
    func setSendLaterOn() {
        saveInt(10, key: SYNCLATERKEY)
    }
    
    func setSendLaterOff() {
        saveInt(0, key: SYNCLATERKEY)
    }
    
    func hasRecordToSend() -> Bool {
        if readInt(SYNCLATERKEY) == 10 {
            return true
        }
        
        return false
    }
    
    func saveToken(token: String) {
        saveString(token, key: TOKEN)
    }
    
    func getToken() -> String {
        return readString(TOKEN)
    }
    
    func isFirstTimeRunning() -> Bool {
        if readInt(FIRSTRUN) == 10 {
            return false
        }
        
        return true
    }
    
    func setAppOpened() {
        saveInt(10, key: FIRSTRUN)
    }
    
    func getUserName() -> String {
        return readString(USERNAME)
    }
    
    func saveUserName(name: String) {
        saveString(name, key: USERNAME)
    }
    
    private func saveInt(value: Int, key: String) {
        NSUserDefaults.standardUserDefaults().setInteger(value, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    private func readInt(key: String) -> Int {
        return NSUserDefaults.standardUserDefaults().integerForKey(key)
    }
    
    private func saveString(value: String, key: String) {
        NSUserDefaults.standardUserDefaults().setValue(value, forKey: key)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    private func readString(key:String) -> String {
        if let str = NSUserDefaults.standardUserDefaults().stringForKey(key) {
            return str
        }
        
        return ""
    }
}