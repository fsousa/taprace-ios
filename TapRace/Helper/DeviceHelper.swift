//
//  DeviceHelper.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/7/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation
import FCUUID


class DeviceHelper {

    class func getDeviceUUID() -> String {
        return FCUUID.uuidForDevice()
    }

    class func getNonce() -> String {
        return FCUUID.uuid()
    }

}