//
//  AppDelegate.swift
//  TapRace
//
//  Created by Felipe Sousa on 1/21/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import NVActivityIndicatorView
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var activityIndicatorView: NVActivityIndicatorView?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

        DDLog.addLogger(DDTTYLogger.sharedInstance())
        DDLog.addLogger(CrashlyticsLogger.sharedInstance())
        
        Fabric.with([Crashlytics.self])
        
        if !StorageHelper.getInstance().isFirstTimeRunning() {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homeViewController = mainStoryboard.instantiateViewControllerWithIdentifier("BaseViewController") as! BaseViewController
            let main = UINavigationController(rootViewController: homeViewController)
            main.navigationBar.hidden = true
            self.window!.rootViewController = main
            
            self.isNetworkAvailable { (available: Bool) -> Void in
                if available {

                    //Generate Token
                    AuthService.sharedInstance().generateToken({
                        (token: String) -> () in
                        StorageHelper.getInstance().saveToken(token)

                        //Send pending record
                        if StorageHelper.getInstance().hasRecordToSend() {
                            let score = StorageHelper.getInstance().readRecord()
                            LeaderboardService.sharedInstance().sendScoreToLeaderboard(score, success: {
                                () -> () in
                                    StorageHelper.getInstance().setSendLaterOff()
                                },
                                failure: {
                                    (error: NSError, statusCode: Int) -> () in
                                    //Do nothing
                            })
                        }else {
                            //Download record.
                            LeaderboardService.sharedInstance().getMyLeaderboard({
                                (leaderBoard: Leaderboard) -> () in
                                    StorageHelper.getInstance().saveRecord(leaderBoard.score)
                                },
                                failure: {
                                    (error: NSError, statusCode: Int) -> () in
                                    DDLogError("error - didFinishLaunchingWithOptions - getMyLeaderboard \(statusCode)")
                            })
                        }
                        },
                        failure: {
                            (error: NSError, statusCode: Int) -> () in
                            DDLogError("error - didFinishLaunchingWithOptions - generateToken \(statusCode)")
                    })
                }
            }
            
        }
        
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func showActivityIndicator(view: UIView) {
        
        self.activityIndicatorView = NVActivityIndicatorView(frame: view.frame,
            type: .LineScaleParty, color: UIColor.blackColor())
        
        view.addSubview(self.activityIndicatorView!)
        self.activityIndicatorView!.startAnimation()
    }
    
    func stopActivityIndicator() {
        activityIndicatorView?.stopAnimation()
    }
    
    func isNetworkAvailable(answer: (available: Bool) -> Void) {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags.ConnectionAutomatic
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            answer(available: false)
            return
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        answer(available:(isReachable && !needsConnection))
    }
    
}

