//
//  Leaderboard.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/7/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation


class Leaderboard {
    
    var name: String!
    var score: Int!
    
    init(name: String, score: Int) {
        self.name = name
        self.score = score
    }
    
}