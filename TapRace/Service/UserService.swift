//
//  UserService.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/17/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation
import AFNetworking

class UserService: Service {
    
    class func sharedInstance() -> UserService {
        struct Static {
            static let instance : UserService = UserService()
        }
        return Static.instance
    }
    
    func createUser(name: String, success:() -> (), failure:(error:NSError, statusCode: Int) -> ()) {
        let manager = super.getRequestOperationManager()
        
        let parameters = [
            "client_key" : DeviceHelper.getDeviceUUID(),
            "name" : name
        ]
        
        manager.POST(super.getServer() + super.getApiVersion() + "/users/create.json", parameters: parameters,
            success: {
                (operation:AFHTTPRequestOperation?, responseObject:AnyObject!) in
                success()
            },
            failure: {
                (operation: AFHTTPRequestOperation?, error: NSError!) in
                let statusCode = operation != nil && operation!.response != nil ? operation!.response!.statusCode : 500
                failure(error: error, statusCode: statusCode)
            }
        )
    }
}