//
//  LeaderboardService.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/7/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation
import AFNetworking
import CryptoSwift

class LeaderboardService: Service {
    
    class func sharedInstance() -> LeaderboardService {
        struct Static {
            static let instance : LeaderboardService = LeaderboardService()
        }
        return Static.instance
    }
    
    func getLeaderboard(success:(leaderBoards:[Leaderboard]) -> (), failure:(error:NSError, statusCode: Int) -> ()) {
        let manager = super.getRequestOperationManager()
        
        let parameters = [
            "token" : super.getToken()
        ]
        
        manager.GET(super.getServer() + super.getApiVersion() + "/secure/leaderboards.json", parameters: parameters,
            success: {
                (operation:AFHTTPRequestOperation?, responseObject:AnyObject!) in
                var leaders = [Leaderboard]()
                let leader = responseObject as! NSArray
                for item in leader {
                    let lea = Leaderboard(name: item["name"] as! String, score: item["score"] as! Int)
                    leaders.append(lea)
                }
                
                success(leaderBoards: leaders)
            },
            failure: {
                (operation: AFHTTPRequestOperation?, error: NSError!) in
                let statusCode = operation != nil && operation!.response != nil ? operation!.response!.statusCode : 500
                failure(error: error, statusCode: statusCode)
            }
        )
    }
    
    func getMyLeaderboard(success:(leaderBoard:Leaderboard) -> (), failure:(error:NSError, statusCode: Int) -> ()) {
        let manager = super.getRequestOperationManager()
        
        let parameters = [
            "token" : super.getToken()
        ]
        
        manager.GET(super.getServer() + super.getApiVersion() + "/secure/leaderboards/single.json", parameters: parameters,
            success: {
                (operation:AFHTTPRequestOperation?, responseObject:AnyObject!) in
                
                let leaderResult = responseObject as! NSDictionary
                let leader = Leaderboard(name: leaderResult["name"] as! String, score: leaderResult["score"] as! Int)
                
                
                success(leaderBoard: leader)
            },
            failure: {
                (operation: AFHTTPRequestOperation?, error: NSError!) in
                let statusCode = operation != nil && operation!.response != nil ? operation!.response!.statusCode : 500
                failure(error: error, statusCode: statusCode)
            }
        )
    }
    
    func sendScoreToLeaderboard(score: Int, success:() -> (), failure:(error:NSError, statusCode: Int) -> ()) {
        let manager = super.getRequestOperationManager()
        let url: String = "\(super.getServer())\(super.getApiVersion())/secure/leaderboards.json"
        
        let device = DeviceHelper.getDeviceUUID()
        let nonce = DeviceHelper.getNonce()
        let plainText = "\(device)-\(nonce)-\(score)"
        let encryptedData = CryptoHelper.getInstance().encryptLeaderboardUpdate(plainText)
        
        let parameters = [
            "brunoandmattforcedmetodothis" : encryptedData,
            "token" : super.getToken()
        ]
        
        manager.PUT(url, parameters:parameters,
            success: {
                (operation:AFHTTPRequestOperation?, responseObject:AnyObject!) in
                success()
            },
            failure: {
                (operation: AFHTTPRequestOperation?, error: NSError!) in
                let statusCode = operation != nil && operation!.response != nil ? operation!.response!.statusCode : 500
                failure(error: error, statusCode: statusCode)
                
            }
        )
    }
    
}
