//
//  AuthService.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/17/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation
import AFNetworking

class AuthService: Service {
    
    class func sharedInstance() -> AuthService {
        struct Static {
            static let instance : AuthService = AuthService()
        }
        return Static.instance
    }
    
    func generateToken(success:(token: String) -> (), failure:(error:NSError, statusCode: Int) -> ()) {
        let manager = super.getRequestOperationManager()
        let randomKey = "\(DeviceHelper.getNonce())-\(CryptoHelper.getInstance().getKeyFix())"
        let parameters = [
            "client_key" : DeviceHelper.getDeviceUUID(),
            "secret_key" : CryptoHelper.getInstance().encrypt(randomKey)
        ]
        
        manager.POST(super.getServer() + super.getApiVersion() + "/auth/generate.json", parameters: parameters,
            success: {
                (operation:AFHTTPRequestOperation?, responseObject:AnyObject!) in
                
                let response = responseObject as! NSDictionary
                let token = response["token"] as! String
                success(token: token)
            },
            failure: {
                (operation: AFHTTPRequestOperation?, error: NSError!) in
                let statusCode = operation != nil && operation!.response != nil ? operation!.response!.statusCode : 500
                failure(error: error, statusCode: statusCode)
            }
        )
    }
}