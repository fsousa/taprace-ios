//
//  Service.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/7/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import Foundation
import AFNetworking

class Service {
    
    final func getServer() -> String {
        //return "http://192.168.188.136:3000"
        return "https://gentle-earth-18426.herokuapp.com"
    }
    
    final func getToken() -> String {
        return StorageHelper.getInstance().getToken()
    }
    
    final func getApiVersion() -> String {
        return "/api/v3"
    }
    
    final func getRequestOperationManager() -> AFHTTPRequestOperationManager {
        
        let certificatePath = NSBundle.mainBundle().pathForResource("heroku", ofType: "cer")!
        let certificateData = NSData(contentsOfFile: certificatePath)!
        
        let manager = AFHTTPRequestOperationManager()
        let nonce = DeviceHelper.getNonce() + "-" + CryptoHelper.getInstance().getKeyFixUserAgent()
        let userAgent = CryptoHelper.getInstance().encryptUserAgent(nonce)
        manager.requestSerializer.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        manager.securityPolicy = AFSecurityPolicy(pinningMode: .Certificate)
        manager.securityPolicy.pinnedCertificates = [certificateData]
        
        return manager
    }
}