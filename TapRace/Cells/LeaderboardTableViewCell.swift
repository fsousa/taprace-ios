//
//  LeaderboardTableViewCell.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/7/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit

class LeaderboardTableViewCell: UITableViewCell {
    
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    func show(item: Leaderboard, pos: Int) {
        
        if pos % 2 == 1 {
            self.backgroundColor = UIColor(rgba: "#3A537C")
        }else {
            self.backgroundColor = UIColor(rgba: "#445D86")
        }
        
        positionLabel.text = "\(pos + 1)."
        nameLabel.text = "\(item.name)"
        scoreLabel.text = "\(item.score)"
    }
}
