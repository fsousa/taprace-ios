//
//  ViewController.swift
//  TapRace
//
//  Created by Felipe Sousa on 1/21/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit
import SCLAlertView

class SinglePlayerViewController: BaseViewController {
    
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var tapCounterLabel: UILabel!
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var maxScoreLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    
    
    let tapRec = UITapGestureRecognizer()
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapRec.addTarget(self, action: "tappedView")
        tapView.addGestureRecognizer(tapRec)
        
        resetTimer()
        resetCounterLabel()
        setMaxScore()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.changeBackgroundColor(self.view, view2: self.shadowView)
    }
    
    @IBAction func resetButtonTouched(sender: AnyObject) {
        resetTimer()
        resetCounterLabel()
    }
    
    @IBAction func cancelButtonTouched(sender: AnyObject) {
        super.stopTheGame()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func tappedView(){
        
        if !isGameRunning() {
            super.startTheGame()
            
        }
        let current: Int = Int(tapCounterLabel.text!)!
        tapCounterLabel.text = "\(current + 1)"
    }
    
    func updateTimer() {
        let current = Float(self.timerLabel.text!)!
        if current == 0.0 {
            self.tapRec.enabled = false
            self.tapView.userInteractionEnabled = false
            super.stopTheGame()
            SCLAlertView().showInfo("Time is over", subTitle: "You clicked \(self.tapCounterLabel.text!) times")
            self.updateMaxScore()
        }else {
            self.timerLabel.text = "\(current - 0.1)"
        }
    }
    
    private func resetCounterLabel() {
        tapCounterLabel.text = "0"
    }
    
    private func resetTimer() {
        if isGameRunning() {
            super.stopTheGame()
        }
        self.tapRec.enabled = true
        tapView.userInteractionEnabled = true
        timerLabel.text = "\(super.getFixTimer())"
    }
    
    private func setMaxScore() {
        maxScoreLabel.text = "\(StorageHelper.getInstance().readRecord())"
    }
    
    private func updateMaxScore() {
        let old = Int(maxScoreLabel.text!)!
        let current = Int(tapCounterLabel.text!)!
        
        if current > old {
            maxScoreLabel.text = "\(current)"
            self.sendScore(current)
        }
    }
    
    private func sendScore(score: Int) {
        StorageHelper.getInstance().saveRecord(score)
        StorageHelper.getInstance().setSendLaterOn()
        self.appDelegate.isNetworkAvailable { (available: Bool) -> Void in
            if available {
                AuthService.sharedInstance().generateToken({
                    (token: String) -> () in
                    StorageHelper.getInstance().saveToken(token)
                    LeaderboardService.sharedInstance().sendScoreToLeaderboard(score, success: {
                        () -> () in
                            StorageHelper.getInstance().setSendLaterOff()
                        },
                        failure: {
                            (error: NSError, statusCode: Int) -> () in
                            DDLogError("error - sendScore - sendScoreToLeaderboard \(statusCode)")
                    })
                    
                    },
                    failure: {
                        (error: NSError, statusCode: Int) -> () in
                        DDLogError("error - sendScore - generateToken \(statusCode)")
                })
            }
        }
    }
    
}

