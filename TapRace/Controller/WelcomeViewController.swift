//
//  WelcomeViewController.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/17/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit
import SCLAlertView

class WelcomeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var userNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        StorageHelper.getInstance().setAppOpened()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 15
    }
    
    func validateName(name: String) -> Bool {
        
        if name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "" {
            return false
        }
        if name.characters.count < 3 {
            return false
        }
        
        return true
    }
    
    @IBAction func doneTouched(sender: UIButton) {
        //TODO Create User
        
        let name: String = userNameTextField.text!
        if !self.validateName(name) {
            SCLAlertView().showError("Sorry", subTitle: "Try a different name.")
            return
            
        }
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.isNetworkAvailable { (available: Bool) -> Void in
            
            if available {
                appDelegate.showActivityIndicator(self.view)
                UserService.sharedInstance().createUser(name,  success: {() -> () in
                    StorageHelper.getInstance().saveUserName(name)
                    AuthService.sharedInstance().generateToken({
                        (token: String) -> () in
                        StorageHelper.getInstance().saveToken(token)
                        let score = StorageHelper.getInstance().readRecord()
                        if score != 0 {
                            LeaderboardService.sharedInstance().sendScoreToLeaderboard(score, success: {
                                () -> () in
                                appDelegate.stopActivityIndicator()
                                self.performSegueWithIdentifier("showMainMenu", sender: self)
                                },
                                failure: {
                                    (error: NSError, statusCode: Int) -> () in
                                    DDLogError("error - refreshLeaderboard - sendScoreToLeaderboard \(statusCode)")
                            })
                        }else {
                            appDelegate.stopActivityIndicator()
                            self.performSegueWithIdentifier("showMainMenu", sender: self)
                        }
                        },
                        failure: {
                            (error: NSError, statusCode: Int) -> () in
                            DDLogError("error - done touched - generateToken \(statusCode)")
                            appDelegate.stopActivityIndicator()
                    })
                    },
                    failure: {
                        (error: NSError, statusCode: Int) -> () in
                        if statusCode == 422 {
                            StorageHelper.getInstance().saveUserName(name)
                            AuthService.sharedInstance().generateToken({
                                (token: String) -> () in
                                StorageHelper.getInstance().saveToken(token)
                                
                                //Download record.
                                LeaderboardService.sharedInstance().getMyLeaderboard({
                                    (leaderBoard: Leaderboard) -> () in
                                    appDelegate.stopActivityIndicator()
                                    StorageHelper.getInstance().saveRecord(leaderBoard.score)
                                    self.performSegueWithIdentifier("showMainMenu", sender: self)
                                    },
                                    failure: {
                                        (error: NSError, statusCode: Int) -> () in
                                        appDelegate.stopActivityIndicator()
                                        if statusCode == 404 {
                                            self.performSegueWithIdentifier("showMainMenu", sender: self)
                                        }else {
                                            SCLAlertView().showError("Oops", subTitle: "Something went wrong. Please try again later. :(")
                                            DDLogError("error - done touched - getMyLeaderboard \(statusCode)")
                                        }
                                })
                                },
                                failure: {
                                    (error: NSError, statusCode: Int) -> () in
                                    DDLogError("error - done touched - generateToken2 \(statusCode)")
                                    appDelegate.stopActivityIndicator()
                            })
                            
                        }else {
                            DDLogError("error - done touched - createUser \(statusCode)")
                            appDelegate.stopActivityIndicator()
                            SCLAlertView().showError("Oops", subTitle: "Something went wrong. Please try again later. :(")
                        }
                })
                
            }
        }
        
    }
    
}
