//
//  BaseViewController.swift
//  TapRace
//
//  Created by Felipe Sousa on 1/23/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class BaseViewController: UIViewController {
    
    private var timer: Float = 15.0
    private var isRunning: Bool = false
    private var countdown: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func startTheGame() {
        if self.isRunning == false {
            self.isRunning = true
            self.countdown = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)
        }
    }
    
    func stopTheGame() {
        if self.isRunning == true {
            self.isRunning = false
            self.countdown.invalidate()
        }
    }
    
    func isGameRunning() -> Bool {
        return self.isRunning
    }
    
    func getFixTimer() -> Float {
        return self.timer
    }
    
    func changeBackgroundColor(view: UIView) {
        let color = ColorHelper.getInstance().getRandomSetColor()
        view.backgroundColor = UIColor(rgba: color[0])
    }
    
    func changeBackgroundColor(view: UIView, view2: UIView) {
        let color = ColorHelper.getInstance().getRandomSetColor()
        view.backgroundColor = UIColor(rgba: color[0])
        view2.backgroundColor = UIColor(rgba: color[1])
    }
    
}
