//
//  MultiPlayerViewController.swift
//  TapRace
//
//  Created by Felipe Sousa on 1/23/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit
import SCLAlertView

class MultiPlayerViewController: BaseViewController {
    
    @IBOutlet weak var player1TapView: UIView!
    @IBOutlet weak var player1CounterLabel: UILabel!
    @IBOutlet weak var timerPlayer1: UILabel!
    @IBOutlet weak var player1NameLabel: UILabel!
    @IBOutlet weak var player1ResetButton: UIButton!
    @IBOutlet weak var pressAndHoldLabelPlayer1: UILabel!
    
    @IBOutlet weak var player2TapView: UIView!
    @IBOutlet weak var player2CounterLabel: UILabel!
    @IBOutlet weak var timerPlayer2: UILabel!
    @IBOutlet weak var pressAndHoldLabelPlayer2: UILabel!
    
    @IBOutlet weak var divisionProgressBar: UIProgressView!
    
    let tapRecPlayer1 = UITapGestureRecognizer()
    let tapRecPlayer2 = UITapGestureRecognizer()
    
    var player1Ready: Bool = false
    var player2Ready: Bool = false
    
    var prepareCountdown: NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.applyLabelTransformation()
        
        tapRecPlayer1.addTarget(self, action: "tappedViewPlayer1")
        player1TapView.addGestureRecognizer(tapRecPlayer1)
        
        tapRecPlayer2.addTarget(self, action: "tappedViewPlayer2")
        player2TapView.addGestureRecognizer(tapRecPlayer2)
        
        divisionProgressBar.setProgress(0.0, animated: false)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        super.changeBackgroundColor(self.view)
    }
    
    func tappedViewPlayer1() {
        
        if !super.isGameRunning() && !player1Ready {
            player1Ready = true
            pressAndHoldLabelPlayer1.text = "READY"
            
            if player2Ready {
                startCountdown()
            }
            
            return
        }
        
        if super.isGameRunning() {
            let current: Int = Int(player1CounterLabel.text!)!
            let newPoint = current + 1
            player1CounterLabel.text = "\(newPoint)"
        }
        
    }
    
    func tappedViewPlayer2() {
        
        if !super.isGameRunning() && !player2Ready {
            player2Ready = true
            pressAndHoldLabelPlayer2.text = "READY"
            
            if player1Ready {
                startCountdown()
            }
            return
        }
        
        if super.isGameRunning() {
            let current: Int = Int(player2CounterLabel.text!)!
            let newPoint = current + 1
            player2CounterLabel.text = "\(newPoint)"
        }
    }
    
    func startCountdown() {
        pressAndHoldLabelPlayer1.text = "PREPARE"
        pressAndHoldLabelPlayer2.text = "PREPARE"
        
        self.prepareCountdown = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: Selector("updateProgressBar"), userInfo: nil, repeats: true)
    }
    
    func updateProgressBar() {
        let current = divisionProgressBar.progress
        
        if current == 1.0 {
            self.divisionProgressBar.setProgress(0.0, animated: false)
            self.prepareCountdown.invalidate()
            super.startTheGame()
            self.changeGameModeToStarted()
            
            return
        }
        
        divisionProgressBar.setProgress(current + 0.04, animated: false)
    }
    
    func changeGameModeToStarted() {
        pressAndHoldLabelPlayer1.hidden = true
        pressAndHoldLabelPlayer2.hidden = true
        
        player1CounterLabel.hidden = false
        player2CounterLabel.hidden = false
        
    }
    
    func updateTimer() {
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let timerP1 = Float(self.timerPlayer1.text!)!
            let timerP2 = Float(self.timerPlayer2.text!)!
            if timerP1 == 0.0 || timerP2 == 0.0 {
                super.stopTheGame()
                self.showWinnerAlert()
            }else {
                self.timerPlayer1.text = "\(timerP1 - 0.1)"
                self.timerPlayer2.text = "\(timerP2 - 0.1)"
            }
        }
    }
    
    private func showWinnerAlert() {
        let p1Score = Int(player1CounterLabel.text!)!
        let p2Score = Int(player2CounterLabel.text!)!
        
        var winner = 0
        if p1Score > p2Score {
            winner = 1
        }else if p1Score < p2Score {
            winner = 2
        }
        
        if winner != 0 {
            SCLAlertView().showInfo("Time is over", subTitle: "Player \(winner) won.")
        }else {
            SCLAlertView().showInfo("Time is over", subTitle: "It was a draw")
        }
        
    }
    
    private func applyLabelTransformation() {
        let transformation = CGAffineTransformMakeRotation( ( 180 * 3.14 ) / 180 );
        player1CounterLabel.transform = transformation
        timerPlayer1.transform = transformation
        player1NameLabel.transform = transformation
        player1ResetButton.transform = transformation
        pressAndHoldLabelPlayer1.transform = transformation
    }
    
    
    @IBAction func resetTouched(sender: AnyObject) {
        
        super.stopTheGame()
        
        self.player1CounterLabel.text = "0"
        self.player1CounterLabel.hidden = true
        
        self.player2CounterLabel.text = "0"
        self.player2CounterLabel.hidden = true
        
        self.timerPlayer1.text = "\(super.getFixTimer())"
        self.timerPlayer2.text = "\(super.getFixTimer())"
        
        self.pressAndHoldLabelPlayer1.text = "Tap when ready."
        self.pressAndHoldLabelPlayer1.hidden = false
        self.pressAndHoldLabelPlayer2.text = "Tap when ready."
        self.pressAndHoldLabelPlayer2.hidden = false
        
        self.player1Ready = false
        self.player2Ready = false
        
        self.divisionProgressBar.setProgress(0.0, animated: false)
        
        if prepareCountdown != nil && prepareCountdown.valid {
            prepareCountdown.invalidate()
        }
        
    }
    
    @IBAction func quitTouched(sender: AnyObject) {
        super.stopTheGame()
        
        if prepareCountdown != nil && prepareCountdown.valid {
            prepareCountdown.invalidate()
        }
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
