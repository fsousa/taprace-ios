//
//  LeaderboardViewController.swift
//  TapRace
//
//  Created by Felipe Sousa on 2/6/16.
//  Copyright © 2016 Fsousasilva. All rights reserved.
//

import UIKit
import SCLAlertView

class LeaderboardViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var leadersTableView: UITableView!
    @IBOutlet weak var noItemsLabel: UILabel!
    
    var leaders: [Leaderboard] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.refreshLeaderboard()
    }
    
    func sortLeaderboard(item1: Leaderboard, item2: Leaderboard) -> Bool {
        if item1.score == item2.score {
            return item1.name < item2.name
        }
        return item1.score > item2.score
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let leaderCell = tableView.dequeueReusableCellWithIdentifier("leaderboardCell") as! LeaderboardTableViewCell
        let leaderItem = getLeaderboardDataSource()[indexPath.row]
        
        leaderCell.show(leaderItem, pos: indexPath.row)
        
        return leaderCell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getLeaderboardDataSource().count
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func getLeaderboardDataSource() -> [Leaderboard] {
        return self.leaders
    }
    
    func refreshLeaderboard() {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.isNetworkAvailable { (available: Bool) -> Void in
            
            if available {
                appDelegate.showActivityIndicator(self.view)
                
                //Send pending record
                if StorageHelper.getInstance().hasRecordToSend() {
                    let score = StorageHelper.getInstance().readRecord()
                    AuthService.sharedInstance().generateToken({
                        (token: String) -> () in
                        StorageHelper.getInstance().saveToken(token)
                        LeaderboardService.sharedInstance().sendScoreToLeaderboard(score, success: {
                            () -> () in
                                appDelegate.stopActivityIndicator()
                                StorageHelper.getInstance().setSendLaterOff()
                            },
                            failure: {
                                (error: NSError, statusCode: Int) -> () in
                                appDelegate.stopActivityIndicator()
                                DDLogError("error - refreshLeaderboard - sendScoreToLeaderboard \(statusCode)")
                        })
                        
                        },
                        failure: {
                            (error: NSError, statusCode: Int) -> () in
                            appDelegate.stopActivityIndicator()
                            DDLogError("error - refreshLeaderboard - generateToken \(statusCode)")
                    })
                }
                
                LeaderboardService.sharedInstance().getLeaderboard({
                    (leaderBoards: [Leaderboard]) -> () in
                    self.leaders = leaderBoards.sort(self.sortLeaderboard)
                    if self.leaders.count != 0 {
                        self.noItemsLabel.hidden = true
                    }
                    appDelegate.stopActivityIndicator()
                    self.leadersTableView.reloadData()
                },
                failure: {
                    (error: NSError, statusCode: Int) -> () in
                    appDelegate.stopActivityIndicator()
                    SCLAlertView().showError("Oops", subTitle: "Something went wrong. Please try again later. :(")
                    DDLogError("error - refreshLeaderboard - getLeaderboard \(statusCode)")
                })
            }else {
                SCLAlertView().showNotice("Are you connected ?", subTitle: "Please check your connection and try again.")
            }
        }
    }
    
    @IBAction func reloadTouched(sender: UIButton) {
        self.refreshLeaderboard()
    }
    @IBAction func exitTouched(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}
